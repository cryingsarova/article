/**
 * ArticlesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

  list:function(req, res){
    Articles.find({}).exec(function(err, articles){
      if(err){
        res.send(500, {error:'Database error'});
      }
      res.view('list', {articles:articles});
    });
  },

  add:function(req, res){
    res.view('add');
  },

  findByTitle:function(req, res){
     var title = req.body.title;
     if (title) {
      Articles.find({title: title}).exec(function(err, articles){
        if(err){
          res.send(500, {error:'Database error'});
        }
        res.view('list', {articles:articles});
      });
     } else {
       Articles.find({}).exec(function(err, articles){
        if(err){
          res.send(500, {error:'Database error'});
        }
        res.view('list', {articles:articles});
      });
    }
  },

  findByAuthor:function(req, res){
     var author = req.body.author;
     if (author) {
      Articles.find({author: author}).exec(function(err, articles){
        if(err){
          res.send(500, {error:'Database error'});
        }
        res.view('list', {articles:articles});
      });
     } else {
       Articles.find({}).exec(function(err, articles){
        if(err){
          res.send(500, {error:'Database error'});
        }
        res.view('list', {articles:articles});
      });
    }
  },

  create:function(req, res){
        var title = req.body.title;
        var body = req.body.body;
        var author = req.body.author;

        Articles.create({title:title, body:body, author:author}).exec(function(err){
            if(err){
                res.send(500, {error: 'Database Error'});
            }

            res.redirect('/articles/list');
        });
    },

  remove: function(req, res){
        var query;
        query = { "id": req.param('id') }
        Articles.destroy(query).exec(function(err){
            if(err){
                res.send(500, {error: 'Database Error'});
            }
            res.redirect('/articles/list');
        });

        return false;
    },

  edit: function(req, res){
        var query;
        query = { "id": req.param('id') }
        Articles.findOne(query).exec(function(err, article){
            if(err){
                res.send(500, {error: 'Database Error'});
            }

            res.view('edit', {article:article});
        });
    },

  update: function(req, res){
        var title = req.body.title;
        var body = req.body.body;

        Articles.update({id: req.params.id},{title:title, body:body}).exec(function(err){
            if(err){
                res.send(500, {error: 'Database Error'});
            }

            res.redirect('/articles/list');
        });

        return false;
    }

};

